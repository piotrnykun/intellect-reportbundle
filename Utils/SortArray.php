<?php

namespace Intellect\ReportBundle\Utils;


class SortArray
{
    private $array_sort, $column_sort, $sort_type, $method_sort, $first_row;
    private $choice_sort = ['asc', 'desc'];

    public function __construct($array_sort, $column_sort, $sort_type)
    {
        $this->array_sort = $array_sort;
        $this->first_row = array_shift($array_sort);
        $this->column_sort = $this->returnColumnIfExists($column_sort);
        $this->sort_type = $this->checkSortType($sort_type);
        $this->method_sort = $this->getSortMethod();
    }

    private function returnColumnIfExists($column_sort)
    {
        $columns = array_keys($this->first_row);
        if(in_array($column_sort, $columns, true)){
            return $column_sort;
        }
        throw new \Exception('Column not exists');
    }

    private function checkSortType($sort_type)
    {
        $sort_type = strtolower($sort_type);
        if(in_array($sort_type, $this->choice_sort, true)) {
            return $sort_type;
        }
        return array_shift($this->choice_sort);
    }

    private function sortString($a, $b)
    {
        if($this->sort_type === 'asc') {
            return strcmp ($a[$this->column_sort],$b[$this->column_sort]);
        }
        return strcmp ($b[$this->column_sort],$a[$this->column_sort]);
    }

    private function sortNumberOrDatetime($a, $b)
    {
        if($this->sort_type === 'asc') {
            return $a[$this->column_sort] > $b[$this->column_sort];
        }
        return $a[$this->column_sort] < $b[$this->column_sort];
    }

    private function getSortMethod()
    {
        if( is_numeric($this->first_row[$this->column_sort])
            || $this->first_row[$this->column_sort] instanceof \DateTime
        ) {
            return 'sortNumberOrDatetime';
        }
        return 'sortString';
    }

    public function getSortArray()
    {
        uasort($this->array_sort, array($this, $this->method_sort));
        return $this->array_sort;
    }
}