<?php

namespace Intellect\ReportBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait Report
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Intellect\ReportBundle\Entity\ReportType")
     * @ORM\JoinColumn(name="report_type_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $report_type;

    /**
     * @ORM\ManyToMany(targetEntity="Intellect\ReportBundle\Entity\ReportColumn", cascade={"persist"})
     * @ORM\JoinTable(name="intellect_reports_reports_columns",
     *      joinColumns={@ORM\JoinColumn(
     *          name="report_id",
     *          referencedColumnName="id",
     *          onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(
     *          name="report_column_id",
     *          referencedColumnName="id",
     *          unique=true,
     *          onDelete="CASCADE")}
     *      )
     */
    private $report_column;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->report_column = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set report_type
     *
     * @param \Intellect\ReportBundle\Entity\ReportType $reportType
     * @return Report
     */
    public function setReportType(\Intellect\ReportBundle\Entity\ReportType $reportType = null)
    {
        $this->report_type = $reportType;

        return $this;
    }

    /**
     * Get report_type
     *
     * @return \Intellect\ReportBundle\Entity\ReportType 
     */
    public function getReportType()
    {
        return $this->report_type;
    }

    /**
     * Add report_column
     *
     * @param \Intellect\ReportBundle\Entity\ReportColumn $reportColumn
     * @return Report
     */
    public function addReportColumn(\Intellect\ReportBundle\Entity\ReportColumn $reportColumn)
    {
        $this->report_column[] = $reportColumn;

        return $this;
    }

    /**
     * Remove report_column
     *
     * @param \Intellect\ReportBundle\Entity\ReportColumn $reportColumn
     */
    public function removeReportColumn(\Intellect\ReportBundle\Entity\ReportColumn $reportColumn)
    {
        $this->report_column->removeElement($reportColumn);
    }

    /**
     * Get report_column
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReportColumn()
    {
        return $this->report_column;
    }
}
