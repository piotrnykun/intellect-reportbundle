<?php

namespace Intellect\ReportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Report
 *
 * @ORM\Table(name="intellect_report")
 * @ORM\Entity(repositoryClass="Intellect\ReportBundle\Repository\ReportRepository")
 */
class Report
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Intellect\ReportBundle\Entity\ReportType")
     * @ORM\JoinColumn(name="report_type_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $report_type;

    /**
     * @ORM\ManyToMany(targetEntity="Intellect\ReportBundle\Entity\ReportColumn")
     * @ORM\JoinTable(name="intellect_reports_reports_columns",
     *      joinColumns={@ORM\JoinColumn(
     *          name="report_id",
     *          referencedColumnName="id",
     *          onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(
     *          name="report_column_id",
     *          referencedColumnName="id",
     *          unique=true,
     *          onDelete="CASCADE")}
     *      )
     */
    protected $report_column;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->report_column = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set report_type
     *
     * @param \Intellect\ReportBundle\Entity\ReportType $reportType
     * @return Report
     */
    public function setReportType(\Intellect\ReportBundle\Entity\ReportType $reportType = null)
    {
        $this->report_type = $reportType;

        return $this;
    }

    /**
     * Get report_type
     *
     * @return \Intellect\ReportBundle\Entity\ReportType 
     */
    public function getReportType()
    {
        return $this->report_type;
    }
}
