<?php

namespace Intellect\ReportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportColumnType
 *
 * @ORM\Table(name="intellect_report_column_type",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="unique_column_type_per_report", columns={"type", "report_type_id"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="Intellect\ReportBundle\Repository\ReportColumnTypeRepository")
 */
class ReportColumnType
{
    const COLUMN_TYPE = [
        'datetime',
        'string',
        'int',
        'float',
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="string", length=50)
     */
    private $type;

    /**
     * @ORM\Column(name="column_type", type="string", length=15)
     */
    private $column_type;

    /**
     * @ORM\ManyToOne(targetEntity="Intellect\ReportBundle\Entity\ReportType", inversedBy="report_column_type")
     * @ORM\JoinColumn(name="report_type_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $report_type;

    /**
     * @ORM\OneToMany(targetEntity="Intellect\ReportBundle\Entity\ReportColumn", mappedBy="report_column_type")
     */
    private $report_column;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->report_column = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set type
     *
     * @param string $type
     * @return ReportColumnType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set column_type
     *
     * @param string $columnType
     * @return ReportColumnType
     */
    public function setColumnType($columnType)
    {
        $this->column_type = $columnType;

        return $this;
    }

    /**
     * Get column_type
     *
     * @return string 
     */
    public function getColumnType()
    {
        return $this->column_type;
    }

    /**
     * Set report_type
     *
     * @param \Intellect\ReportBundle\Entity\ReportType $reportType
     * @return ReportColumnType
     */
    public function setReportType(\Intellect\ReportBundle\Entity\ReportType $reportType = null)
    {
        $this->report_type = $reportType;

        return $this;
    }

    /**
     * Get report_type
     *
     * @return \Intellect\ReportBundle\Entity\ReportType 
     */
    public function getReportType()
    {
        return $this->report_type;
    }

    /**
     * Add report_column
     *
     * @param \Intellect\ReportBundle\Entity\ReportColumn $reportColumn
     * @return ReportColumnType
     */
    public function addReportColumn(\Intellect\ReportBundle\Entity\ReportColumn $reportColumn)
    {
        $this->report_column[] = $reportColumn;

        return $this;
    }

    /**
     * Remove report_column
     *
     * @param \Intellect\ReportBundle\Entity\ReportColumn $reportColumn
     */
    public function removeReportColumn(\Intellect\ReportBundle\Entity\ReportColumn $reportColumn)
    {
        $this->report_column->removeElement($reportColumn);
    }

    /**
     * Get report_column
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReportColumn()
    {
        return $this->report_column;
    }
}
