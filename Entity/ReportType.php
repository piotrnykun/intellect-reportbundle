<?php

namespace Intellect\ReportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportType
 *
 * @ORM\Table(name="intellect_report_type")
 * @ORM\Entity(repositoryClass="Intellect\ReportBundle\Repository\ReportTypeRepository")
 */
class ReportType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="string", length=50, unique=true)
     */
    private $type;

    /**
     * @ORM\Column(name="label", type="string", length=500)
     */
    private $label;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Intellect\ReportBundle\Entity\ReportColumnType",
     *     mappedBy="report_type",
     *     cascade={"persist"}
     *     )
     */
    private $report_column_type;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->report_column_type = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set type
     *
     * @param string $type
     * @return ReportType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return ReportType
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Add report_column_type
     *
     * @param \Intellect\ReportBundle\Entity\ReportColumnType $reportColumnType
     * @return ReportType
     */
    public function addReportColumnType(\Intellect\ReportBundle\Entity\ReportColumnType $reportColumnType)
    {
        $this->report_column_type[] = $reportColumnType;

        return $this;
    }

    /**
     * Remove report_column_type
     *
     * @param \Intellect\ReportBundle\Entity\ReportColumnType $reportColumnType
     */
    public function removeReportColumnType(\Intellect\ReportBundle\Entity\ReportColumnType $reportColumnType)
    {
        $this->report_column_type->removeElement($reportColumnType);
    }

    /**
     * Get report_column_type
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReportColumnType()
    {
        return $this->report_column_type;
    }
}
