# ReportBundle

## Instalacja
1. Dodać do *composer.json* wpisy:
```json    
    "repositories": [
        {
            "type": "vcs",
            "url": "git@git.nln.pl:jszafranski/intellect-reportbundle.git"
        }
    ],
     "require": {
             "intellect/reportbundle":"master-dev"
         },
```